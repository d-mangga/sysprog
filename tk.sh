#!/bin/bash

function pause(){
	local message="Press [Enter] key to continue..."
	read -p "$message" readEnterKey
}

function show_menu(){
    date
    echo "---------------------------"
    echo "   Main Menu"
    echo "---------------------------"
    echo "1. List of Available WIFI"
    echo "2. List of Available ETHERNATE INTERFACES"
    echo "3. Get IP Address for Specific PORT"
    echo "4. Turn ON/OFF Ethernet Port"
    echo "5. Connect to WIFI"
    echo "6. Disconnect from WIFI"
    echo "7. Manipulate e1000 Driver"
    echo "8. Ping test"
    echo "9. Exit"
}

function write_header(){
	local h="$@"
	echo "---------------------------------------------------------------"
	echo "     ${h}"
	echo "---------------------------------------------------------------"
}

function get_ethernet_port(){

	ports=$(ip link | awk -F: '$0 !~ "lo|vir|wl|^[^0-9]"{print $2;getline}')
	count=0
	for port in $ports
		do
			status=$(ip link show | grep $port | tr -s " " | cut -d " " -f9)
			echo "$count) $port : $status"
			count=count+1
		done
}

function available_wifi(){
	write_header "LIST OF AVAILABLE WIFI"
	nmcli d wifi list
	pause
}

function available_port(){
		write_header "LIST OF YOUR PORT INTERFACES"
		# ip link show
		get_ethernet_port
		pause
}

function connect(){
	local a
	local b
	write_header "LIST OF AVAILABLE WIFI"
	nmcli d wifi list
	read -p "SSID: " a
	read -p "PASSWORD: " b
	nmcli dev wifi connect $a password $b
	pause
}

function disconnect(){
	write_header "ACTIVE CONNECTION PORT"
	nmcli device status
	local a
	read -p "INTERFACE to disconnect: " a
	sudo nmcli dev disconnect  $a
	pause
}

function get_ip() {
	write_header "LIST OF ETHERNET PORT"
	get_ethernet_port
	read -p "Type your desired interface " c
	while [[ c -gt ${#ports[@]}-1 ]];
	do
		echo "Check your input!"
		read -p "Type your desired interface " c
	done	
	if=${ports[c]}
	write_header "IP ADDRESS $if"
	echo "$if : $(ip -4 addr show $if | grep inet | tr -s " " | cut -d " " -f3)"
	pause
}

function turn_on_off_port(){
	write_header "YOUR ACTION"
	local a
	local b
	echo "0 => Turn off selected port"
	echo "1 => Turn on selected port"
	read -p "Your Choice: " a
	while !([ $a == 0 ] || [ $a == 1 ]);
	do
		echo "Check your action number!"
		read -p "Your choice: " a
	done
	write_header "LIST OF ETHERNET PORT"
	get_ethernet_port
	read -p "Interface to manipulate: " b
	while [[ b -gt ${#ports[@]}-1 ]];
	do
		echo "Check your input!"
		read -p "Interface to manipulate: " b
	done
	if=${ports[b]}
	case $a in
		0) sudo ip link set $if down ;;
		1) sudo ip link set $if up ;;
		*) 
		   echo "Unkwown choice :("
	           pause
	esac
	write_header "AFTER ACTION"
	get_ethernet_port
	pause
}

function custom_e1000() {	
	sudo rmmod e1000
	sudo modprobe e1000 AutoNeg=1 RxIntDelay=60000
	echo "AutoNeg=1 and RxIntDelay=60000 configured"
	sleep 2
	dmesg | tail
	pause
}

function reset_e1000() {
	sudo rmmod e1000
	sudo modprobe e1000
	echo "e1000 config resetted"
	sleep 2
	dmesg | tail
	pause
}

function manipulate_e1000() {
	write_header "MANIPULATE e1000 Driver"
	dmesg | tail
	local a
	echo "0 => set to default config"
	echo "1 => set AutoNeg=1 and RxIntDelay=60000"
	read -p "your choice: " a
	case $a in
		0) reset_e1000 ;;
		1) custom_e1000 ;;
		*) 
		   echo "unknown command :("
		   pause
	esac 	
}

function ping_test() {
	write_header "TEST PING"
	local a
	read -p "type your desired address to test ping: " a
	ping $a -c 3
	pause
}

function read_input(){
	local c
	read -p "choose 1 - 9 " c
	case $c in
		1)	available_wifi ;;
		2)	available_port ;;
		3)  get_ip ;;
		4) 	turn_on_off_port ;;
		5)	connect ;;
		6)	disconnect ;;
		7) 	manipulate_e1000 ;;
		8) 	ping_test ;;
		9)	echo "Bye!"; running=false ;;
		*)	
			echo "Please select between 1 to 9 choice only."
			pause
	esac
}

running=true
while $running
do
	clear
	show_menu
 	read_input
done
